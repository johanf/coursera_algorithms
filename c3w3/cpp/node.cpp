//
// Created by johan on 2018-03-21.
//
#include "node.h"


Node::Node(int val){
    this->val = val;
};

Node::Node(const Node &other){
    this->right = other.right;
    this->left = other.left;
    this->val = other.val;
}
Node::Node(){
    this->right = nullptr;
    this->left = nullptr;
    this-> val = 0;
}
