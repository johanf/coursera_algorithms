//
// Created by johan on 2018-03-21.
//

#include <queue>
#include <vector>
#include <iostream>
#include "node.h"
#include <limits>


void binary_insert(Node &root, double val){
    if (root.val < val) {
        if (root.right == nullptr) {
            Node child(val);
            root.right = &child;
        } else {
            binary_insert(*root.right, val);
        }
    }
    else{
        if (root.left == nullptr){
            Node child(val);
            root.left = &child;
        }
        else{
            binary_insert(*root.left, val);
        }
    }
}
int max_depth;
int min_depth = 1e6;

void print_tree(Node node, int depth = 0)
{
    if (node.left != nullptr){
        print_tree(*node.left, depth + 1);
    }
    if ((node.left == nullptr) and (node.right == nullptr)){
        if (depth > max_depth){
            max_depth = depth;
        }
        if (depth < min_depth){
            min_depth = depth;
        }
    }
    if ((node.right != nullptr)){
        print_tree(*node.right, depth +1 );
    }

}
//Not used.
void del_tree(Node* node)
{
    if (node->left != nullptr){
        del_tree(node->left);
    }
    if (node->right != nullptr){
        del_tree(node->right);
    }
    delete node;

}


int main(){


    auto cmp = [](Node n1, Node n2) { return n1.val > n2.val;};
    std::priority_queue<Node, std::vector<Node>, decltype(cmp)> q(cmp);

    //std::vector<int> numbers {0,1,2,3,4,5};
    std::vector<int> numbers {37, 59, 43, 27, 30, 96, 96, 71, 8,76};
    //std::vector<int> numbers {1,2};

    for(auto n:numbers){
        Node N(n);
        q.push(N);
    }
    std::vector<Node*> trash;   //Contains pointers to New nodes...
    while (q.size() > 1){

        Node* n1 = new Node(q.top());
        q.pop();

        Node* n2 = new Node(q.top());
        q.pop();

        double sum_val = n1->val + n2->val;

        Node s_node(sum_val);
        s_node.left = n1;
        s_node.right = n2;
        q.push(s_node);

        trash.push_back(n1);
        trash.push_back(n2);


    }

    max_depth = -1;
    min_depth = std::numeric_limits<int>::max();

    Node root = q.top();
    q.pop();
    print_tree(root);


    std::cout << "max depth: " << max_depth << std::endl;
    std::cout << "min depth: " << min_depth << std::endl;


    for(auto n:trash){
        delete n;
    }
};