//
// Created by johan on 2018-03-21.
//

#ifndef CPP_NODE_H
#define CPP_NODE_H

#endif //CPP_NODE_H



class Node
{
public:
    double val;
    Node* left = nullptr;
    Node* right = nullptr;


    Node(int val);
    Node(const Node&);
    Node();

};