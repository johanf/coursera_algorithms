import heap

def read_file(filename='median.txt'):
    with open(filename) as f:
        L = f.read()
        L = L.split()
        L = [int(l) for l in L]
    return L

L = read_file()

left_heap = heap.MaxHeap()
right_heap = heap.Heap()

medians = []

for i in range(len(L)):
    x_i = L[i]
    #print(x_i)
    low_med = left_heap.get_root()
    up_med = right_heap.get_root()

    if x_i > up_med:
        right_heap.add_task(x_i, x_i)
    else:
        left_heap.add_task(x_i, x_i)

    if len(left_heap) - len(right_heap) > 1:
        xv = left_heap.pop_task()
        right_heap.add_task(xv, xv)

    if -len(left_heap) + len(right_heap) > 1:
        xv = right_heap.pop_task()
        left_heap.add_task(xv, xv)

    if len(left_heap) >= len(right_heap):
        medians.append(left_heap.get_root())
    else:
        medians.append(right_heap.get_root())

# print("Left heap:")
# for i in range(len(left_heap)):
#     print(left_heap.pop_task())
#
# print("Right heap:")
#
# for i in range(len(right_heap)):
#     print(right_heap.pop_task())
#
#







