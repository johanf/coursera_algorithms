import heapq
from itertools import count


class Heap:
    def __init__(self):
        self.entry_finder = {}               # mapping of tasks to entries
        self.REMOVED = '<removed-task>'      # placeholder for a removed task
        self.counter = count()     # unique sequence count
        self.pq = []
        self.N = 0

    def get_root(self):
        try:
            return self.pq[0][0]
        except:
            print("Heap is empty!")
            return float('nan')

    def add_task(self, task, priority=0):
        'Add a new task or update the priority of an existing task'
        if task in self.entry_finder:
            self.remove_task(task)
            self.N -= 1
        count = next(self.counter)
        entry = [priority, count, task]
        self.entry_finder[task] = entry
        heapq.heappush(self.pq, entry)
        self.N += 1

    def remove_task(self, task):
        'Mark an existing task as REMOVED.  Raise KeyError if not found.'
        entry = self.entry_finder.pop(task)
        entry[-1] = self.REMOVED
        self.N -= 1

    def pop_task(self):
        'Remove and return the lowest priority task. Raise KeyError if empty.'
        while self.pq:
            priority, count, task = heapq.heappop(self.pq)
            if task is not self.REMOVED:
                del self.entry_finder[task]
                self.N -= 1
                return task
        raise KeyError('pop from an empty priority queue')

    def __len__(self):
        return self.N



class MaxHeap(Heap):

    def add_task(self, task, priority=0):
        'Add a new task or update the priority of an existing task'
        if task in self.entry_finder:
            self.remove_task(task)
            self.N -= 1
        count = next(self.counter)
        entry = [-priority, count, task]
        self.entry_finder[task] = entry
        heapq.heappush(self.pq, entry)
        self.N += 1
    def get_root(self):
        try:
            return -self.pq[0][0]
        except:
            print("Heap is empty!")
            return float('nan')


