import graph
import heapq
from itertools import count
import time

def read_file(filename='data.txt'):
    g = graph.DdGraph()
    with open(filename) as f:
        all_edges = f.readlines()
        for edges in all_edges:
            edges = edges.split()
            e1 = int(edges[0]) - 1
            for edge in edges[1::]:
                e2, w = edge.split(',')
                e2, w = int(e2) - 1, int(w)
                g.add_edge(e1, e2, w)
                g.add_edge(e2, e1, w)
                #print((e1, e2, w))
    return g

#argmin = lambda L: min(range(len(L)), key=lambda x: L[x])

def argmin(Q, d):
    min_val = float('inf')
    min_ind = float('nan')
    ind = float('nan')
    for i in range(len(Q)):
        q = Q[i]
        if d[q] < min_val:
            min_val = d[q]
            min_ind = q
            ind = i
    return (min_ind, ind)


                  # list of entries arranged in a heap
entry_finder = {}               # mapping of tasks to entries
REMOVED = '<removed-task>'      # placeholder for a removed task
counter = count()     # unique sequence count
pq = []

def add_task(task, priority=0):
    'Add a new task or update the priority of an existing task'
    if task in entry_finder:
        remove_task(task)
    count = next(counter)
    entry = [priority, count, task]
    entry_finder[task] = entry
    heapq.heappush(pq, entry)


def remove_task(task):
    'Mark an existing task as REMOVED.  Raise KeyError if not found.'
    entry = entry_finder.pop(task)
    entry[-1] = REMOVED


def pop_task():
    'Remove and return the lowest priority task. Raise KeyError if empty.'
    while pq:
        priority, count, task = heapq.heappop(pq)
        if task is not REMOVED:
            del entry_finder[task]
            return task
    raise KeyError('pop from an empty priority queue')


def dijkstra(g, source):
    """

    :param g: Graf to be used in dijkstra
    :param source: Starting node.
    """
    REMOVED = '<removed-task>'

    N = len(g)
    Q = list(range(N))  # list of nodes
    V = range(N)
    d = [float("inf")]*N
    prev = [float('nan')]*N

    d[source] = 0

    for ii in range(N):
        add_task(V[ii], d[ii])

    while N:

        u = pop_task()
        #print("d: ", d)
        #print("Q:", Q)
        #print(("u,ind"), (u, ind))
        N = N - 1
        for v, d_uv in g.adjacent_edges(u).items():
            if v not in Q:
                continue

            #print("(u,v) = ", (u,v), "dist = ", d_uv)

            alt = d[u] + d_uv
            if alt < d[v]:
                d[v] = alt
                prev[v] = u
                add_task(v, alt)

    return d, prev


g = read_file('data.txt')

tic = time.time()
for i in range(100):

    source = 0
    (d, prev) = dijkstra(g, source)

toc = time.time()
print("priority que took: %.2f" % ((toc - tic)/100))

L = [7,37,59,82,99,115,133,165,188,197]

for i in range(len(L)):
    L[i] = d[L[i] - 1]

print(L)

