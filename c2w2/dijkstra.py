import graph


def read_file(filename='data.txt'):
    g = graph.DdGraph()
    with open(filename) as f:
        all_edges = f.readlines()
        for edges in all_edges:
            edges = edges.split()
            e1 = int(edges[0]) - 1
            for edge in edges[1::]:
                e2, w = edge.split(',')
                e2, w = int(e2) - 1, int(w)
                g.add_edge(e1, e2, w)
                g.add_edge(e2, e1, w)
                #print((e1, e2, w))
    return g

#argmin = lambda L: min(range(len(L)), key=lambda x: L[x])

def argmin(Q, d):
    min_val = float('inf')
    min_ind = float('nan')
    ind = float('nan')
    for i in range(len(Q)):
        q = Q[i]
        if d[q] < min_val:
            min_val = d[q]
            min_ind = q
            ind = i
    return (min_ind, ind)



def dijkstra(g, source):
    """

    :param g: Graf to be used in dijkstra
    :param source: Starting node.
    """

    N = len(g)
    Q = list(range(N))  # list of nodes
    V = range(N)
    d = [float("inf")]*N
    prev = [float('nan')]*N

    d[source] = 0

    while Q:

        (u,ind) = argmin(Q, d)
        #print("d: ", d)
        #print("Q:", Q)
        #print(("u,ind"), (u,ind))
        del Q[ind]

        for v, d_uv in g.adjacent_edges(u).items():
            if v not in Q:
                continue

            #print("(u,v) = ", (u,v), "dist = ", d_uv)

            alt = d[u] + d_uv
            if alt < d[v]:
                d[v] = alt
                prev[v] = u

    return d, prev

g = read_file('data.txt')

source = 0
(d, prev) = dijkstra(g, source)

L = [7,37,59,82,99,115,133,165,188,197]

for i in range(len(L)):
    L[i] = d[L[i] - 1]

print(L)