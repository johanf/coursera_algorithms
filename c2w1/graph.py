# -*- coding: utf-8 -*-
"""
Created on Sun Mar  4 20:51:56 2018

@author: Johan
"""
from collections import defaultdict
import sys
from collections import deque
from collections import Counter
import cProfile


def argsort(seq):
    return sorted(range(len(seq)), key=seq.__getitem__)

def graph_file(filename = 'scc.txt'):
    g = dd_graph()
    with open(filename) as f:
        edges = f.readlines()
        edges = [x.strip() for x in edges]

    for l in edges:
        i, j = l.split()
        try:
            g.add_edge(int(i) - 1 , int(j) - 1)  #lowest index is 1 in file.
        except:
            print("error in parcing")
    return g

class dd_graph:

    def __init__(self):
        self.A = defaultdict(list)
        self.N = 0

    def add_edge(self, start, end):
        self.A[start].append(end)

    def construct_graph(self):
        max_v = -1
        for key, a_list in self.A.items():
            max_size = max(max(a_list), key)
            if max_size>max_v:
                max_v = max_size

        self.N = max(len(self.A.values()), max_v + 1)
        self.discovered = self.N*[False]

    def visited(self,v):
        self.discovered[v] = True

    def isvisited(self,v):
        try:
            return self.discovered[v]
        except:
            print("too big: ", v, "len(discovered): ", len(self.discovered))
            sys.exit("Aborting!")

    def nextnode(self):
        for i in range(self.N):
            if not self.discovered[i] :
                return i
        return -1

    def adjentEdges(self,v):
        return self.A[v]

    def delEdge(self,start, end):
        del self.A[start][end]

    def __str__(self):
        return str(self.A)

    def __len__(self):
        return self.N

    def reverse(self):
        A_rev = defaultdict(list)
        for start in self.A:
            for end in self.A[start]:
                A_rev[end].append(start)

        self.A = A_rev
        return self

    '''This is a recursive implementation,
       not used, gives overflow'''

def DFS(g, v, cc_flag = False, cc_dic = {}, cc_num = 0):
    g.visited(v)
    if cc_flag:
        if cc_dic.get(v,-1) == -1:
            cc_dic[v] = cc_num
    print(cc_flag, cc_dic, cc_num)
    print("visting node: ", v)
    for n in g.adjentEdges(v):
        if not g.isvisited(n):
            DFS(g,n, cc_flag, cc_dic, cc_num)
    if v not in L:
        L.insert(0,v)

def all_explored(g, v):
    for w in g.adjentEdges(v):
        if not g.isvisited(w):
            return False
    return True


def dfs1_iter(g, v, L):

    que = deque()
    que.append(v)

    while que:
        v = que[-1]

        global count
        count = count + 1
        if not (count % 10000):
            print(" %.2f" % (sum(g.discovered)/len(g.discovered)))
            print("len(que): ", len(que))
            print()

        if g.isvisited(v):
            que.pop()
            global L_count
            if L[v] == -1:
                L[v] = L_count
                L_count = L_count + 1

        else:
            for n in g.adjentEdges(v):
                if not g.isvisited(n):
                    que.append(n)
        g.visited(v)


def dfs2_iter(g, v, cc_dic, cc_num):

    que = deque()
    que.append(v)

    while que:
        v = que[-1]

        global count
        count = count + 1
        if not (count % 10000):
            print(" %.2f" % (sum(g.discovered)/len(g.discovered)))
            print("len(que): ", len(que))
            print()

        if g.isvisited(v):
            que.pop()
        else:
            for n in g.adjentEdges(v):
                if not g.isvisited(n):
                    que.append(n)
            cc_dic[v] = cc_num
            g.visited(v)


if False:
    g = dd_graph()
    g.add_edge(0, 1)
    g.add_edge(1, 2)
    g.add_edge(2, 0)
    g.add_edge(2, 3)
    g.add_edge(3, 4)
    g.add_edge(4, 5)
    g.add_edge(5, 3)
    g.add_edge(3, 6)
    g.add_edge(6, 7)
    g.add_edge(7, 6)
    g.add_edge(7, 8)

else:
    g = graph_file()

g.construct_graph()
g.reverse()    #First iteration if Kosaraju's is a reverse DFS

L = [-1]*len(g)  # Global list of finishing times
L_count = 0      # Counter for finishing times

count = 1        # To measure "progress"
N = len(g)
for i in range(N):
    if not g.isvisited(i):
        dfs1_iter(g, i, L)


L = argsort(L)
L.reverse()

g.reverse()
g.construct_graph()    # resets visited.

cc_num = 0
cc_dic = {}
for l in L:
    if not g.isvisited(l):
        dfs2_iter(g, l, cc_dic, cc_num)
        cc_num = cc_num + 1    #This was crazy much faster!

c = Counter()
for keys, vals in cc_dic.items():
    c[vals] += 1

print(c.most_common(5))
result = c.most_common(5)
scc5 = [k[1] for k in result]
print(scc5)
